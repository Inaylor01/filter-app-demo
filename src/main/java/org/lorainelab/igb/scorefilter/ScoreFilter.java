/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.scorefilter;

import aQute.bnd.annotation.component.Component;
import com.affymetrix.genometry.BioSeq;
import com.affymetrix.genometry.Scored;
import com.affymetrix.genometry.filter.SymmetryFilterI;
import com.affymetrix.genometry.general.BoundedParameter;
import com.affymetrix.genometry.general.IParameters;
import com.affymetrix.genometry.general.Parameter;
import com.affymetrix.genometry.general.Parameters;
import com.affymetrix.genometry.operator.comparator.MathComparisonOperator;
import com.affymetrix.genometry.parsers.FileTypeCategory;
import com.affymetrix.genometry.symmetry.impl.SeqSymmetry;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author noorzahara
 */
@Component(immediate = true)
public class ScoreFilter implements SymmetryFilterI, IParameters {

    private final static String SCORE = "score";
    private final static String COMPARATOR = "comparator";
    private final static float DEFAULT_SCORE = 10;
    private final static List<MathComparisonOperator> COMPARATOR_VALUES = new LinkedList<>();
    protected Parameters parameters;

    private final Parameter<Float> score = new Parameter<>(DEFAULT_SCORE);
    private final Parameter<MathComparisonOperator> comparator = new BoundedParameter<>(COMPARATOR_VALUES);

    static {
        COMPARATOR_VALUES.add(new com.affymetrix.genometry.operator.comparator.GreaterThanEqualMathComparisonOperator());
        COMPARATOR_VALUES.add(new com.affymetrix.genometry.operator.comparator.GreaterThanMathComparisonOperator());
        COMPARATOR_VALUES.add(new com.affymetrix.genometry.operator.comparator.EqualMathComparisonOperator());
        COMPARATOR_VALUES.add(new com.affymetrix.genometry.operator.comparator.LessThanMathComparisonOperator());
        COMPARATOR_VALUES.add(new com.affymetrix.genometry.operator.comparator.LessThanEqualMathComparisonOperator());
        COMPARATOR_VALUES.add(new com.affymetrix.genometry.operator.comparator.NotEqualMathComparisonOperator());
    }

    public ScoreFilter() {
        //super();
        parameters = new Parameters();
        parameters.addParameter(SCORE, Float.class, score);
        parameters.addParameter(COMPARATOR, MathComparisonOperator.class, comparator);
    }

    @Override
    public boolean filterSymmetry(BioSeq seq, SeqSymmetry sym) {
        if (sym instanceof Scored) {
            float s = ((Scored) sym).getScore();
            return Float.compare(s, Scored.UNKNOWN_SCORE) != 0 && comparator.get().operate(s, (float) score.get());
        }
        return false;
    }

    @Override
    public String getName() {
        return SCORE;
    }

    @Override
    public String getPrintableString() {
        return SCORE + " " + comparator.get().getDisplay() + " " + score.get();
    }

    @Override
    public String getDisplay() {
        return SCORE;
    }

    @Override
    public boolean isFileTypeCategorySupported(FileTypeCategory fileTypeCategory) {
        return fileTypeCategory == FileTypeCategory.Annotation
                || fileTypeCategory == FileTypeCategory.Alignment
                || fileTypeCategory == FileTypeCategory.ProbeSet;
    }

    @Override
    public SymmetryFilterI newInstance() {
        try {
            SymmetryFilterI newInstance = getClass().getConstructor().newInstance();
            if (newInstance instanceof IParameters) {
                getParametersType().keySet().forEach((key) -> {
                    ((IParameters) newInstance).setParameterValue(key, getParameterValue(key));
                });
            }
            return newInstance;
        } catch (Exception ex) {
        }
        return null;
    }

    @Override
    public Map<String, Class<?>> getParametersType() {
        return parameters.getParametersType();
    }

    @Override
    public boolean setParametersValue(Map<String, Object> params) {
        return parameters.setParametersValue(params);
    }

    @Override
    public boolean setParameterValue(String key, Object value) {
        return parameters.setParameterValue(key, value);
    }

    @Override
    public Object getParameterValue(String key) {
        return parameters.getParameterValue(key);
    }

    @Override
    public List<Object> getParametersPossibleValues(String key) {
        return parameters.getParametersPossibleValues(key);
    }

}
